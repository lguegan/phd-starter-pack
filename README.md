# PhD Starter Pack

Dear PhD Students!  
You just started your PhD yesterday and you have a couple of unanswered questions ?  
This repo could help you.  
Please read el famoso **paper.pdf**.

# You still have questions ? Suggestions ?

We can talk together on irc:
- Server: **freenode**
- Channel: **#phd-starter-pack**
- [Instant access](https://webchat.freenode.net/)
